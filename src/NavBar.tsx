import { NavLink } from 'react-router-dom';
import './styles.css';

export function NavBar() {
    return (
        <nav className="navbar-container">
            <NavLink to="/">Home</NavLink>
            <NavLink to="search">Search</NavLink>
            <NavLink to="translate">Translate</NavLink>
            {/* <NavLink to="Players">Players</NavLink> */}
        </nav>
    );
}
