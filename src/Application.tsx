import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LayoutComponent from './components/Layout';
import HomePage from './pages/Home';
import {Translate } from './pages/Translate';
import { NavBar } from './NavBar';
import { Provider } from 'react-redux';
import { store } from './state';
import { Search } from './components/Search';
import 'bootstrap/dist/css/bootstrap.min.css';


export interface IApplicationProps {}

const Application: React.FunctionComponent<IApplicationProps> = (props) => {
    return (
         <Provider store={store}>
        <BrowserRouter>

        <NavBar />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="search">
                    <Route index element={<Search />} />
                    <Route path=":number" element={<Search />} />
                </Route>
                <Route path="translate" element={<Translate />} />
            </Routes>
        </BrowserRouter>

         </Provider>
    );
};

export default Application;
