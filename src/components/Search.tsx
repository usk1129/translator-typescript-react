import { Provider } from 'react-redux';
import { store } from '../state';
import RepositoriesList from '../pages/RepositoriesList';

export const Search = () => {
    return (
        <Provider store={store}>
            <>
                <div >
                    <h1>Search for a package</h1>
                </div>
                <div>&nbsp;</div>
                <RepositoriesList />
            </>
        </Provider>
    );
};

export default Search;
