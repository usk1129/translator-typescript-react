import React from 'react';


export interface IHomePageProps {}

const HomePage: React.FunctionComponent<IHomePageProps> = (props) => {
    return (
        <div>
            <h1 style={{ textAlign: 'center' }}>This is the home page</h1>
            &nbsp;
            <h3 style={{ textAlign: 'center' }}>React project app for searching npm packages and translate code </h3>
        </div>
    );
};

export default HomePage;
