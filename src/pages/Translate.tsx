import * as esbuild from 'esbuild-wasm';
import { useState, useEffect, useRef } from 'react';
import TextareaAutosize from '@mui/base/TextareaAutosize';

export const Translate = () => {
    const ref = useRef<any>();
    const [input, setInput] = useState('');
    const [code, setCode] = useState('');
    const text = 'example code: const App = () => <div>Hi! </div>';
    const startService = async () => {
        ref.current = await esbuild.startService({
            worker: true,
            wasmURL: '/esbuild.wasm'
        });
    };
    useEffect(() => {
        startService();
    }, []);

    const onClick = async () => {
        if (!ref.current) {
            return;
        }

        const result = await ref.current.transform(input, {
            loader: 'jsx',
            target: 'es2015'
        });

        setCode(result.code);
    };

    return (
        <>
            <h1>Translate Code</h1>
            &nbsp;
            <div>
                {/* <textarea value={input} placeholder={text} onChange={(e) => setInput(e.target.value)}></textarea> */}
                <TextareaAutosize maxRows={100} aria-label="maximum height" value={input} placeholder={text} onChange={(e) => setInput(e.target.value)} style={{ width: 1000, height: 100 }} />
                <div>
                    <button onClick={onClick}>Submit</button>
                </div>
                <pre>{code}</pre>
            </div>
        </>
    );
};
